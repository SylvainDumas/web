package uri

import (
	"bytes"
	"encoding/base64"
	"errors"
	"mime"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"

	mt "gitlab.com/SylvainDumas/web/mediatype"
)

// https://fr.wikipedia.org/wiki/Sch%C3%A9ma_d%27URI

// https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
// https://en.wikipedia.org/wiki/List_of_URI_schemes
// URI = scheme:[//authority]path[?query][#fragment]
// authority = [userinfo@]host[:port]

var ErrUnsupportedOutputType = errors.New("Unsupported output type")

type ReaderFn func(*url.URL, interface{}) error

var (
	mutex   sync.RWMutex
	readers map[string]ReaderFn
)

func init() {
	readers = make(map[string]ReaderFn)
	Register("data", dataReader)
	Register("file", fileReader)
	Register("http", httpReader)
	Register("https", httpReader)
}

func Register(scheme string, functor ReaderFn) {
	mutex.RLock()
	defer mutex.RUnlock()
	readers[strings.ToLower(scheme)] = functor
}

func ReadFrom(src string, out interface{}) error {
	uri, err := url.Parse(src)
	if err != nil {
		return err
	}

	mutex.RLock()
	defer mutex.RUnlock()

	functor, found := readers[uri.Scheme]
	if found == false {
		return errors.New("Not supported")
	}

	return functor(uri, out)
}

// ___________________________________ data ___________________________________
// https://en.wikipedia.org/wiki/Data_URI_scheme
// data:[<media type>][;base64],<data>

func dataReader(src *url.URL, out interface{}) (err error) {
	// Split to get media type part and data part
	var separator = strings.LastIndex(src.Path, ",")
	var data = []byte(src.Path[separator+1:])
	var mediatype = src.Path[:separator]
	// If base64 extension present, decode data and remove ;base64 element of media type part
	if strings.HasSuffix(mediatype, ";base64") {
		// 7 = len(";base64")
		mediatype = mediatype[:len(mediatype)-7]
		data, err = base64.StdEncoding.DecodeString(string(data))
		if err != nil {
			return err
		}
	}

	// If no media type defined, set default value
	if mediatype == "" {
		mediatype = "text/plain;charset=US-ASCII"
	}

	// Decode to out following media type
	return mt.DecodeContent(mediatype, bytes.NewReader(data), out)
}

// ___________________________________ file ___________________________________
// https://en.wikipedia.org/wiki/File_URI_scheme
// file://host/path

func fileReader(src *url.URL, out interface{}) (err error) {
	var filename = src.Path
	if src.Host != "" && src.Host != "localhost" {
		filename = "//" + src.Host + "/" + filename
	}

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	return mt.DecodeContent(mime.TypeByExtension(filepath.Ext(filename)), file, out)
}

// ___________________________________ http / https ___________________________________

func httpReader(*url.URL, interface{}) error {
	return errors.New("Not implemented")
}
