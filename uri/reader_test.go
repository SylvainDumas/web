package uri

import "testing"

func Test_fileReader(t *testing.T) {
	ReadFrom(`file:///c:/path/to/the%20file.txt`, nil)
	ReadFrom(`file:\\remotehost\share\dir\file.txt`, nil)
	ReadFrom(`file://hostname/path/to/the%20file.txt`, nil)
}
