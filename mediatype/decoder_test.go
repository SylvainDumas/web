package mediatype

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"
)

func compareSliceStrings(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func Test_decoders(t *testing.T) {
	var check = func(outValue string) decoderFn {
		return func(mediaType MediaType, body io.Reader, out interface{}) error {
			*out.(*string) = outValue
			return nil
		}
	}

	// Check registration
	Decoders.RegisterByTypeSubtype("type/subtype", check("type/subtype"))
	if _, found := Decoders.typeSubtypes["type/subtype"]; found == false {
		t.Error("Bad register of type/subtype")
	}
	Decoders.RegisterBySubtype("subtype", check("subtype"))
	if _, found := Decoders.subtypes["subtype"]; found == false {
		t.Error("Bad register of subtype")
	}

	// Check registration replace
	var value string
	Decoders.RegisterByTypeSubtype("type/subtype", check("typeReplace/subtypeReplace"))
	Decoders.typeSubtypes["type/subtype"](MediaType{}, nil, &value)
	if value != "typeReplace/subtypeReplace" {
		t.Error("Bad register replacement of type/subtype")
	}
	Decoders.RegisterBySubtype("subtype", check("subtypeReplace"))
	Decoders.subtypes["subtype"](MediaType{}, nil, &value)
	if value != "subtypeReplace" {
		t.Error("Bad register replacement of subtype")
	}

	// Check get decoder
	decoder, err := Decoders.getDecoder(MediaType{DataType: "xxx", Format: Format{SubType: "xxx"}})
	if err == nil {
		t.Error("an error must occured for unknown content type xxx/xxx")
	}

	decoder, err = Decoders.getDecoder(MediaType{DataType: "type", Format: Format{SubType: "subtype"}})
	if err != nil {
		t.Error("decoder for content type type/subtype must be found")
	}
	var dataDecoded string
	if err = decoder(MediaType{}, nil, &dataDecoded); err != nil {
		t.Errorf("decoder function error: %v", err)
	}
	if dataDecoded != "typeReplace/subtypeReplace" {
		t.Errorf("bad decoder function getted: %v", dataDecoded)
	}

	decoder, err = Decoders.getDecoder(MediaType{DataType: "xxx", Format: Format{SubType: "subtype"}})
	if err != nil {
		t.Error("decoder for content type xxx/subtype must be found")
	}
	if err = decoder(MediaType{}, nil, &dataDecoded); err != nil {
		t.Errorf("decoder function error: %v", err)
	}
	if dataDecoded != "subtypeReplace" {
		t.Errorf("bad decoder function getted: %v", dataDecoded)
	}
}

func Test_decodeBody(t *testing.T) {
	var inMapInterface = map[string]interface{}{
		"a": "family",
	}
	inRawMap, _ := json.Marshal(inMapInterface)

	// Check nil
	if err := decodeBody(nil, ioutil.NopCloser(bytes.NewReader(inRawMap)), nil); err != nil {
		t.Error(err)
	}

	// Check []byte
	var outBytes []byte
	if err := decodeBody(nil, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outBytes); err != nil {
		t.Error(err)
	} else if bytes.Equal([]byte(`{"a":"family"}`), outBytes) == false {
		t.Errorf(`in: {"a":"family"} - out: %s`, outBytes)
	}

	// Check bad content type
	var outMapInterface map[string]interface{}
	if err := decodeBody(nil, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outMapInterface); err == nil {
		t.Error("an error must occur for missing content type with out structure")
	}
	var httpHeaders = make(http.Header)
	httpHeaders.Set(contentTypeHeader, "/json; charset=UTF-8")
	if err := decodeBody(httpHeaders, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outMapInterface); err == nil {
		t.Error("an error must occur for missing content type with out structure")
	}

	// Check unregistered subtype decoder
	httpHeaders.Set(contentTypeHeader, "application/prs.foo; charset=UTF-8")
	if err := decodeBody(httpHeaders, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outMapInterface); err == nil {
		t.Error("an error must occur for unknown content type with out structure")
	}

	// Check registered json subtype decoder
	httpHeaders.Set(contentTypeHeader, "application/json; charset=UTF-8")
	if err := decodeBody(httpHeaders, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outMapInterface); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(inMapInterface, outMapInterface) == false {
		t.Errorf("in: %+v - out: %+v", inMapInterface, outMapInterface)
	}
}

func Test_jsonContentTypeDecoder(t *testing.T) {
	var inMapInterface = map[string]interface{}{
		"a": "family",
	}
	inRawMap, _ := json.Marshal(inMapInterface)

	// Check json.RawMessage
	var outRawMessage json.RawMessage
	if err := jsonContentTypeDecoder(MediaType{}, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outRawMessage); err != nil {
		t.Error(err)
	} else if bytes.Equal([]byte(`{"a":"family"}`), outRawMessage) == false {
		t.Errorf(`in: {"a":"family"} - out: %s`, outRawMessage)
	}

	// Check map[string]interface{}
	var outMapInterface map[string]interface{}
	if err := jsonContentTypeDecoder(MediaType{}, ioutil.NopCloser(bytes.NewReader(inRawMap)), &outMapInterface); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(inMapInterface, outMapInterface) == false {
		t.Errorf("in: %+v - out: %+v", inMapInterface, outMapInterface)
	}
}

func Test_xmlContentTypeDecoder(t *testing.T) {
	blob := `
	<animals>
		<animal>gopher</animal>
		<animal>armadillo</animal>
		<animal>zebra</animal>
		<animal>unknown</animal>
		<animal>gopher</animal>
		<animal>bee</animal>
		<animal>gopher</animal>
		<animal>zebra</animal>
	</animals>`
	var attended = []string{"gopher", "armadillo", "zebra", "unknown", "gopher", "bee", "gopher", "zebra"}

	var zoo struct {
		Animals []string `xml:"animal"`
	}
	if err := xmlContentTypeDecoder(MediaType{}, ioutil.NopCloser(strings.NewReader(blob)), &zoo); err != nil {
		t.Error(err)
	} else if compareSliceStrings(zoo.Animals, attended) == false {
		t.Errorf("in: %v - out: %v", attended, zoo.Animals)
	}
}

func Test_textPlainContentTypeDecoder(t *testing.T) {
	inString := "hello world"

	// Check string
	var outString string
	if err := textPlainContentTypeDecoder(MediaType{}, ioutil.NopCloser(strings.NewReader(inString)), &outString); err != nil {
		t.Error(err)
	} else if inString != outString {
		t.Errorf("in: %v - out: %v", inString, outString)
	}

	// Check unsupported
	var outBytes []byte
	if err := textPlainContentTypeDecoder(MediaType{}, ioutil.NopCloser(strings.NewReader(inString)), &outBytes); err == nil {
		t.Error("An error for unsupported out type must occured")
	}
}
