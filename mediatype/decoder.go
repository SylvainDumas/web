package mediatype

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
)

// Decoders contains all registered content type body decoder
var Decoders decoders

func init() {
	Decoders.typeSubtypes = make(map[string]decoderFn)
	Decoders.subtypes = make(map[string]decoderFn)

	Decoders.RegisterBySubtype("json", jsonContentTypeDecoder)
	Decoders.RegisterBySubtype("xml", xmlContentTypeDecoder)

	Decoders.RegisterByTypeSubtype("text/plain", textPlainContentTypeDecoder)
}

// ___________________________ Decoders ___________________________

type decoderFn func(MediaType, io.Reader, interface{}) error

type decoders struct {
	mutex        sync.RWMutex
	typeSubtypes map[string]decoderFn // by pair type/subtype (type data/format data) decoders
	subtypes     map[string]decoderFn // by subtype (format data) decoders
}

func (obj *decoders) RegisterByTypeSubtype(typeSubtype string, functor decoderFn) {
	obj.mutex.Lock()
	defer obj.mutex.Unlock()
	obj.typeSubtypes[strings.ToLower(typeSubtype)] = functor
}

func (obj *decoders) RegisterBySubtype(subtype string, functor decoderFn) {
	obj.mutex.Lock()
	defer obj.mutex.Unlock()
	obj.subtypes[strings.ToLower(subtype)] = functor
}

func (obj *decoders) getDecoder(mediaType MediaType) (decoderFn, error) {
	obj.mutex.RLock()
	defer obj.mutex.RUnlock()

	var typeSubtype = mediaType.DataType + "/" + mediaType.Format.String()

	if decoder, found := obj.typeSubtypes[typeSubtype]; found {
		return decoder, nil
	}

	if decoder, found := obj.subtypes[mediaType.Format.String()]; found {
		return decoder, nil
	}

	return nil, fmt.Errorf("No decoder found for content type: %v", typeSubtype)
}

// ___________________________ Decode body ___________________________

func DecodeContent(mediaTypeStr string, in io.Reader, out interface{}) (err error) {
	switch typed := out.(type) {
	case nil:
		_, err = io.Copy(ioutil.Discard, in)
	case *[]byte:
		*typed, err = ioutil.ReadAll(in)
	default:
		mediaType, err := Decode(mediaTypeStr)
		if err != nil {
			return err
		}

		decoder, err := Decoders.getDecoder(mediaType)
		if err != nil {
			return err
		}

		return decoder(mediaType, in, out)
	}

	return
}

// ___________________________ Decode body ___________________________

func decodeBody(headers http.Header, body io.ReadCloser, out interface{}) (err error) {
	switch typed := out.(type) {
	case nil:
		_, err = io.Copy(ioutil.Discard, body)
	case *[]byte:
		*typed, err = ioutil.ReadAll(body)
	default:
		// Get Content-Type and decode following it
		contentType := headers.Get(contentTypeHeader)
		decodedContentType, err := Decode(contentType)
		if err != nil {
			return err
		}

		decoder, err := Decoders.getDecoder(decodedContentType)
		if err != nil {
			return err
		}

		// TODO pass headers to decoder ?
		return decoder(decodedContentType, body, out)
	}

	return
}

// ___________________________ JSON Content Type Decoder ___________________________

func jsonContentTypeDecoder(mediaType MediaType, body io.Reader, out interface{}) (err error) {
	switch typed := out.(type) {
	case *json.RawMessage:
		*typed, err = ioutil.ReadAll(body)
	default:
		err = json.NewDecoder(body).Decode(out)
	}
	return
}

// ___________________________ XML Content Type Decoder ___________________________

func xmlContentTypeDecoder(mediaType MediaType, body io.Reader, out interface{}) error {
	return xml.NewDecoder(body).Decode(out)
}

// ___________________________ Text/Plain Content Type Decoder ___________________________

func textPlainContentTypeDecoder(mediaType MediaType, body io.Reader, out interface{}) (err error) {
	switch typed := out.(type) {
	case *string:
		contentBody, err := ioutil.ReadAll(body)
		if err == nil {
			*typed = string(contentBody)
		}
	default:
		err = errors.New("Unsupported destination object for content type text/plain")
	}

	return
}
