package mediatype

import "testing"

func Test_Decode(t *testing.T) {
	type MediaTypeParameters map[string]string

	var check = func(value string, dataType string, format Format, parameters MediaTypeParameters) {
		decoded, err := Decode(value)
		if err != nil {
			t.Errorf("%v: %v", value, err)
		}

		// type (data type) check
		if decoded.DataType != dataType {
			t.Errorf("%v: type expected %v - get %v", value, dataType, decoded.DataType)
		}
		// subtype (format type) check
		if decoded.Format != format {
			t.Errorf("%v: subtype expected %v - get %v", value, format, decoded.Format)
		}

		// parameters checks
		if len(decoded.Parameters) != len(parameters) {
			t.Errorf("%v: parameters count expected %v - get %v", value, len(parameters), len(decoded.Parameters))
		}
		for k, v := range parameters {
			decodedValue, keyFound := decoded.Parameters[k]

			if keyFound == false {
				t.Errorf("%v: parameters key %v missing", value, k)
			}
			if decodedValue != v {
				t.Errorf("%v: parameters key %v value expected %v - get %v", value, k, decodedValue, v)
			}
		}

	}

	var checkError = func(value string, msg string) {
		if _, err := Decode(value); err == nil {
			t.Errorf("%v: %v", value, msg)
		}
	}

	// Check good types
	check("application/json", "application", Format{SubType: "json"}, nil)
	check("application/json; charset=UTF-8", "application", Format{SubType: "json"}, MediaTypeParameters{"charset": "UTF-8"})

	// Check different string cases
	check("aPplIcation/jsOn; chArseT=UTF-8", "application", Format{SubType: "json"}, MediaTypeParameters{"charset": "UTF-8"})

	// Check parameters extraction
	check("application/json;", "application", Format{SubType: "json"}, nil)
	check("application/json; ", "application", Format{SubType: "json"}, nil)
	check("application/json;charset=UTF-8", "application", Format{SubType: "json"}, MediaTypeParameters{"charset": "UTF-8"})
	check("application/json  ;  charset=UTF-8  ", "application", Format{SubType: "json"}, MediaTypeParameters{"charset": "UTF-8"})

	// Check strange content type with no type and subtype
	checkError("", "no media type")
	checkError(";", "no media type")
	checkError("  ;  ", "no media type")
	checkError(";  charset=UTF-8  ", "no media type")
	checkError("   ;  charset=UTF-8  ", "no media type")

	// Check missing type or subtype
	checkError("/json; charset=UTF-8", "missing type must be detected")
	checkError("application/; charset=UTF-8", "missing subtype must be detected")
	check("application; charset=UTF-8", "application", Format{}, MediaTypeParameters{"charset": "UTF-8"})

	// Check missing key or value parameter
	checkError("application/json; =UTF-8", "missing key parameter must be detected")
	checkError("application/json; charset=", "missing value parameter must be detected")

	// Check spaces
	check("   aPplIcation/jsOn    ;   chArseT=UTF-8   ", "application", Format{SubType: "json"}, MediaTypeParameters{"charset": "UTF-8"})
	checkError("   aPplIcation  /  jsOn    ;   chArseT  =  UTF-8   ", "expected slash after first token")
	checkError("    /   json   ; charset=UTF-8", "missing type must be detected")
	checkError("   application   /; charset=UTF-8", "missing subtype must be detected")
	check("  application; charset=UTF-8", "application", Format{}, MediaTypeParameters{"charset": "UTF-8"})
	checkError("application/json;    =UTF-8   ", "missing key parameter must be detected")
	checkError("application/json;    charset=   ", "missing value parameter must be detected")
}
