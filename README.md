[![pipeline status](https://gitlab.com/SylvainDumas/web/badges/master/pipeline.svg)](https://gitlab.com/SylvainDumas/web/-/commits/master)
[![coverage report](https://gitlab.com/SylvainDumas/web/badges/master/coverage.svg)](https://gitlab.com/SylvainDumas/web/-/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
